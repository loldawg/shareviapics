/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Share,
  TouchableOpacity
} from 'react-native';

import uploadimage from './functions.js';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component<{}> {
  mitar = () => {
	  const data = new FormData();
	  data.append('name', 'testName');
	  data.append('photo', {
		  uri: 'http://i.pics.rs/8509e.png',
		  type: 'image/jpeg',
		  name: 'testPhotoName'
	  });


	  uploadimage('http://beta.pics.rs/api/pictures', {
		  method: 'post',
		  body: data
	  }, (progressEvent) => {
		  const progress = progressEvent.loaded / progressEvent.total;
		  console.log(progress);
	  }).then((res) => console.log(res), (err) => console.log(err))

  }
  render() {
    return (
      <View style={styles.container}>
<TouchableOpacity onPress={this.mitar}>
		<Text style={styles.welcome}>
          Welcome to Reakt Native!
        </Text>
</TouchableOpacity>
        <Text style={styles.instructions}>
          To get started, edit App.js
        </Text>
        <Text style={styles.instructions}>
          {instructions}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
