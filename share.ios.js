/**
 * Sample React Native Share Extension
 * @flow
 */

import React, { Component } from 'react'
import ShareExtension from 'react-native-share-extension'

import {
  Text,
  View,
  TouchableOpacity
} from 'react-native'
import ProgressBar from 'react-native-progress/Bar'
import uploadimage from './functions.js';

export default class Share extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      type: '',
      value: '',
	  response: '',
	  prog: 0,
	  error: '',
	  shareurl: '',
    }
  }


  async componentDidMount() {

    try {
      const { type, value } = await ShareExtension.data()
	  this.setState({
		  isOpen: true,
		  type,
		  value,
	  });

	  const UPLOAD_URL = 'http://beta.pics.rs/api/pictures';
	  const data = new FormData();

	  data.append('image', {
		  uri: value,
		  type: type,
		  name: `szlika.${type}`
	  });
	  uploadimage(UPLOAD_URL, {
		  method: 'POST',
		  body: data
	  }, (progressEvent) => {
		  const progress = progressEvent.loaded / progressEvent.total;
		  this.setState({
			  prog: progress
		  });
	  }).then((res) => {
		  this.setState({
			  shareurl: `http://beta.pics.rs/i/${JSON.parse(res).data.alphaid}`
		  });
	  })

    }
	catch(e){

	}

  }

  closing = () => ShareExtension.close()


  render() {

    return (

        <View style={{ flex:1 }}>
          	<View style={{ backgroundColor: '#2d3037', flex:1, width: '100%', alignItems: 'center', justifyContent: 'center', }}>
			  <ProgressBar
			  progress = { this.state.prog }
			  width={200}
			  color={'#FFF'}
			  >
			  </ProgressBar>
			  <Text style={{ color: '#FFF', paddingTop: 20 }}> Share: { this.state.shareurl }</Text>
			  <TouchableOpacity style={{ paddingTop: 20 }} onPress={ this.closing }>
			  	<Text style={{ color: '#FFF' }}> Close modal </Text>
			  </TouchableOpacity>
          </View>
        </View>

    );
  }
}
